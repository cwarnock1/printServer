from flask import Flask
from flask import request
from flask import render_template
from datetime import datetime
import board
import serial
import json
import requests
import adafruit_thermal_printer
import textwrap

# printer setup
ThermalPrinter = adafruit_thermal_printer.get_printer_class(2.168)
RX = board.RX
TX = board.TX
uart = serial.Serial("/dev/ttyS0", baudrate=19200, timeout=3000)
printer = ThermalPrinter(uart, auto_warm_up=False)

# setup server
app = Flask(__name__)


@app.route('/')
def main():
    return render_template('main.html')

@app.route('/quotes', methods = ['POST'])
def quote():
    response = requests.get("https://api.quotable.io/random")
    quote = response.json()['content']
    author = response.json()['author']
    string = quote + " -" + author
    wrappedText = textwrap.fill(string, 16)

    now = datetime.now()
    currentTime = now.strftime("%H:%M:%S")

    printer.warm_up()

    # Print large size text.
    printer.size = adafruit_thermal_printer.SIZE_LARGE

    printer.justify = adafruit_thermal_printer.JUSTIFY_CENTER
    printer.inverse = True
    printer.print(currentTime)
    printer.inverse = False
    printer.justify = adafruit_thermal_printer.JUSTIFY_LEFT
    printer.feed(2)

    printer.print(wrappedText)
    printer.feed(5)
    
    # No Content success status response code
    return '', 204

@app.route('/shopping-list', methods = ['POST'])
def shoppingList():
    content = json.loads(request.data)
    print(content);
    
    now = datetime.now()
    currentTime = now.strftime("%m/%d/%y")

    printer.warm_up()

    # Print large size text.
    printer.size = adafruit_thermal_printer.SIZE_LARGE

    printer.justify = adafruit_thermal_printer.JUSTIFY_CENTER
    printer.inverse = True
    printer.print('-SHOPPING LIST-')
    printer.inverse = False
    printer.print(currentTime)
    printer.justify = adafruit_thermal_printer.JUSTIFY_LEFT
    printer.feed(2)
    
    for item in content['list']:
        wrappedText = textwrap.fill("- " + item, 16)
        printer.print(wrappedText)
        printer.feed(1)
        
    
    printer.feed(3)
    
    # No Content success status response code
    return '', 204

if __name__ == "__main__":
    app.run(host='0.0.0.0')