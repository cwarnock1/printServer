function addItemToList() {
    var item = $('#addItem').val();
    console.log(item);

    if (item != '') {
        $("#shoppingList").append('<li value="' + item + '">' + item + '</li>');
        $('#addItem').val('');
    }
}

function printShoppingList() {
    var items = new Object();
    items.list = [];

    $('#shoppingList li').each(function(){
        items.list.push($(this).attr('value'));
    });

    console.log(items);

    $.ajax({
        type: "POST",
        url: '/shopping-list',
        data: JSON.stringify(items),
        contentType: "json",
        dataType: "json",
        success: function() {
            alert('Sent');
        },
        error: function() {
            alert('Error');
        }
    });
}

function printQuote() {
    
    $.ajax({
        type: "POST",
        url: 'quotes',
        success: function() {
            alert('Sent');
        },
        error: function() {
            alert('Error');
        }
    });
}

$( document ).ready(function() {
    
});
